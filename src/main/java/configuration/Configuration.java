package configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuration {

    private final Properties properties = new Properties();
    private final static String LOCAL_PROPERTIES_PATH = "local.properties";

    public Configuration() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File((LOCAL_PROPERTIES_PATH));
        try {
            properties.load(new FileInputStream(file));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getValue(String key) {

        return properties.getProperty(key);
    }
}