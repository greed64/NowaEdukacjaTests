package modules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.mainpages.Page;

import pages.sale.ZarzadzanieSalamiPage;

import java.util.List;

public class NavigationMenu extends Page {

    @FindBy(xpath = "/html/body/div[1]/div/div[2]/ul/li/a")
    private WebElement zarzadzanieSalamiButton;

    @FindBy(xpath = "/html/body/div[1]/div/div[2]/ul/li[2]/a")
    private WebElement perspektywaPelnomocnikaButton;

    public NavigationMenu(WebDriver driver) {
        super(driver);
    }

    public ZarzadzanieSalamiPage goToZarzadzanieSalamiPage(){
        waitForVisibilityWebElement(zarzadzanieSalamiButton);
        zarzadzanieSalamiButton.click();
        return new ZarzadzanieSalamiPage(driver);
    }

}

