package pages.mainpages;

import configuration.Configuration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

//import pages.myaccountpages.*;

public class HomePage extends UserPage {

    private static final Configuration CONFIG = new Configuration();
    private final String url;

    public HomePage(WebDriver driver, String url){
        super(driver);
        this.url = url;
    }

    public HomePage(WebDriver driver){
        super(driver);
        this.url = CONFIG.getValue("app.qa.site");
    }

    public HomePage open(){
        driver.get(url);
        return this;
    }

}
