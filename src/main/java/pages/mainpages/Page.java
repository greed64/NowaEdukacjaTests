package pages.mainpages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {

    protected final WebDriver driver;
    private WebDriverWait wait;
    private Actions action;
    private JavascriptExecutor javaScriptDriver;

    public Page(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 20);
        action = new Actions(driver);
        javaScriptDriver = (JavascriptExecutor) driver;

        PageFactory.initElements(driver, this);
    }

    protected void waitForClickableWebElement(WebElement webElement){
        wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    protected void waitForVisibilityWebElement(WebElement webElement){
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }
}
