package pages.mainpages;

import modules.NavigationMenu;
import org.openqa.selenium.WebDriver;

public abstract class UserPage extends Page {

    protected final NavigationMenu navigationMenu;

    public UserPage(WebDriver driver) {
        super(driver);
        navigationMenu = new NavigationMenu(driver);
    }
}
