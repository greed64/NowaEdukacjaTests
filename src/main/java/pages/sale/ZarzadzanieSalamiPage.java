package pages.sale;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import pages.mainpages.UserPage;

import java.util.List;

/**
 * Created by Greed on 2017-02-07.
 */
public class ZarzadzanieSalamiPage extends UserPage {

    private static String HEADER_TEXT = "Sale";
    private static String CORRECT_URL = "http://nowaedukacjaclient.azurewebsites.net/Sale/Szukaj";

    @FindBy(xpath = "/html/body/div[2]/h2")
    private WebElement zarzadzanieSalamiHeader;

    @FindBy(xpath = "/html/body/div[2]/div/div[1]/div[2]/input")
    private WebElement numerSaliInput;

    @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[2]/input")
    private WebElement budynekInput;

    @FindBy(xpath = "/html/body/div[2]/div/div[3]/div[2]/select")
    private WebElement typSaliInput;

    @FindBy(className = "ms-elem-selectable")
    private List<WebElement> wyposazenieDoWyboruInput;

    @FindBy(id = "ms-selected")
    private List<WebElement> wyposazenieWybraneInput;

    @FindBy(xpath = "/html/body/div[2]/div/div[6]/div/button")
    private WebElement searchButton;

    @FindBy(xpath = "//*[@id=\"saleBody\"]/tr")
    private List<WebElement> sale;

    @FindBy(id = "nowaSalaWlaczacz")
    private WebElement addSalaRow;

    @FindBy(id = "DodajNumerSali")
    private WebElement addSalaNumerSali;

    @FindBy(id = "DodajBudynek")
    private WebElement addSalaBudynek;

    @FindBy(id = "DodajTypSali")
    private WebElement addSalaTypSali;

    @FindBy(id = "DodajLiczbaMiejsc")
    private WebElement addSalaLiczbaMiejsc;

    @FindBy(id = "DodajWyposazenie")
    private WebElement addSalaWyposarzenie;

    @FindBy(id = "dodajNowaSale")
    private WebElement addSalaButton;

    @FindBy(id = "responseDialog")
    private WebElement popupDialog;

    @FindBy(className = "ui-dialog-titlebar-close")
    private WebElement closePopup;


    public ZarzadzanieSalamiPage(WebDriver driver) {
        super(driver);
    }

    public boolean isZarzadzanieSalamiPage(){
        boolean isCorrectHeader = zarzadzanieSalamiHeader.getText().contains(HEADER_TEXT);
        boolean isCorrectURL = driver.getCurrentUrl().contains(CORRECT_URL);

        return isCorrectHeader && isCorrectURL;
    }

    public void searchSala(String numerSali, String budynek){
        numerSaliInput.clear();
        numerSaliInput.sendKeys(numerSali);
        budynekInput.clear();
        budynekInput.sendKeys(budynek);

        searchButton.click();
    }

    public boolean isSalaShowed (String numerSali, String budynek) {
        boolean isNumerSali = false;
        boolean isBudynek = false;
        boolean isSalaFound = false;

        for (WebElement sala : sale) {
            isNumerSali = sala.findElement(By.id("numer")).getText().toLowerCase().contains(numerSali.toLowerCase());
            isBudynek = sala.findElement(By.id("budnek")).getText().toLowerCase().contains(budynek.toLowerCase());

            isSalaFound = isNumerSali && isBudynek;
            if (isSalaFound) {
                return isSalaFound && isZarzadzanieSalamiPage();
            }
        }
        return isSalaFound && isZarzadzanieSalamiPage();
    }

    public void addNewSala(String numerSali, String budynek, String typSali, String liczbaMiejsc, String wyposarzenie){
        addSalaRow.click();
        waitForClickableWebElement(addSalaButton);

        addSalaNumerSali.sendKeys(numerSali);
        addSalaBudynek.sendKeys(budynek);
        addSalaTypSali.findElement(By.id(typSali)).click();
        addSalaLiczbaMiejsc.click();
        addSalaLiczbaMiejsc.sendKeys(liczbaMiejsc);

        addSalaButton.click();
    }

    public boolean isNewSalaAddedCorrect(){

        return popupDialog.findElement(By.className("alert-success")).isDisplayed();
    }

    public void deleteSala(String numerSali, String budynek){
        searchSala(numerSali,budynek);
        sale.get(0).findElement(By.className("btn-danger")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    public boolean isCorrectDeletedSala(String numerSali, String budynek){
        boolean isCorrectPopup = isNewSalaAddedCorrect();
        boolean isNotFoundSala = false;
        closPopup();
        searchSala(numerSali,budynek);
        isNotFoundSala = sale.size() <=2;
        return isCorrectPopup && isNotFoundSala;
    }

    private void closPopup(){
        closePopup.click();
    }

    public  void addFirstWyposazenie(){
        wyposazenieDoWyboruInput.get(0).click();
    }

}
