import org.testng.annotations.DataProvider;

public class DataTestProvider {

    @DataProvider(name="searchSale")
    public static Object[][] getCorrectSearchSaleData() {
        return new Object[][]{
                {"111a","D-2"}};
    }

    @DataProvider(name="newSala")
    public static Object[][] getCorrectSalData() {
        return new Object[][]{
                {"111a","D-2","1","30","2"}};
    }

    @DataProvider(name="deleteSala")
    public static Object[][] getSalaToDelete() {
        return new Object[][]{
                {"111a","D-2"}
        };
    }
}