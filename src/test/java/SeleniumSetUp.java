import configuration.Configuration;
import modules.NavigationMenu;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.mainpages.HomePage;

public abstract class SeleniumSetUp {

    private WebDriver driver;
    protected HomePage homePage;
    protected NavigationMenu navigationMenu;
    private static final Configuration CONFIG = new Configuration();

    @BeforeMethod
    public void openBrowser(){
        System.setProperty("webdriver.chrome.driver", CONFIG.getValue("webdriver.chrome.driver"));
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openSite(){
        homePage = new HomePage(driver, CONFIG.getValue("app.qa.site"))
                .open();
        navigationMenu = new NavigationMenu(driver);
    }

    @AfterMethod
    public void closeBrowser(){
        driver.quit();
    }

    public static String getAdminLogin(){
        return CONFIG.getValue("app.admin.login");
    }

    public static String getAdminPassword(){
        return CONFIG.getValue("app.admin.password");
    }

    public static String getUserLogin(){
        return CONFIG.getValue("app.user.login");
    }

    public static String getUserPassword(){
        return CONFIG.getValue("app.user.password");
    }
}
