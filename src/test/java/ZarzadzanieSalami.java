import org.testng.annotations.Test;
import pages.sale.ZarzadzanieSalamiPage;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Created by Greed on 2017-02-07.
 */
public class ZarzadzanieSalami extends SeleniumSetUp {

    @Test
    public void testShouldDisplayZarzadzanieSalami() {
        ZarzadzanieSalamiPage zarzadzanieSalamiPage = navigationMenu.goToZarzadzanieSalamiPage();

        assertTrue (zarzadzanieSalamiPage.isZarzadzanieSalamiPage());
    }

    @Test(dataProvider="searchSale", dataProviderClass= DataTestProvider.class)
    public void testShouldSearchedSala(String sala, String budynek) {
        ZarzadzanieSalamiPage zarzadzanieSalamiPage = navigationMenu.goToZarzadzanieSalamiPage();
        zarzadzanieSalamiPage.searchSala(sala, budynek);

        assertTrue (zarzadzanieSalamiPage.isSalaShowed(sala, budynek));
    }

    @Test(dataProvider="newSala", dataProviderClass= DataTestProvider.class)
    public void testShouldAddNewSala(String numerSali, String budynek, String typSali, String liczbaMiejsc, String wyposarzenie) {
        ZarzadzanieSalamiPage zarzadzanieSalamiPage = navigationMenu.goToZarzadzanieSalamiPage();
        zarzadzanieSalamiPage.addNewSala(numerSali, budynek, typSali, liczbaMiejsc, wyposarzenie);

        assertTrue (zarzadzanieSalamiPage.isNewSalaAddedCorrect());
    }

    @Test(dataProvider="deleteSala", dataProviderClass= DataTestProvider.class)
    public void testShouldDeleteSala(String numerSali, String budynek) {
        ZarzadzanieSalamiPage zarzadzanieSalamiPage = navigationMenu.goToZarzadzanieSalamiPage();
        zarzadzanieSalamiPage.deleteSala(numerSali, budynek);

        assertTrue (zarzadzanieSalamiPage.isCorrectDeletedSala(numerSali, budynek));
    }
}

